# movement-analyzer

**Студент:** Пономарев Степан Алексеевич, гр P34301.

**Тема проекта:** Разработка системы анализа передвижения людских потоков в помещении.

**Место выполнения проекта:** Университет ИТМО.

**Куратор:** Быковский Сергей Вячеславович, доцент, к.т.н.

**Цель проекта:** Разработка ПО обрабатывающего данные с камер в аудитории и отсылающего управляющие сигналы устройствам в ней.

---

## Источники

1. [C4: Real-time pedestrian detection](https://smartech.gatech.edu/bitstream/handle/1853/44693/ICRA_final.pdf?sequence=1)
2. [Real-Time Human Detection as an Edge Service Enabled by a Lightweight CNN](https://arxiv.org/pdf/1805.00330)
3. [Robust Real Time Multiple Human Detection and Tracking for Automatic Visual Surveillance System](https://wseas.com/journals/sp/2021/a265114-011(2021).pdf)
4. [Real-Time Human Detection using Relational Depth Similarity Features](http://mprg.jp/wp/wp-content/uploads/2013/06/PIA64.pdf)